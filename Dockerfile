FROM eclipse-temurin:22-jdk-alpine
VOLUME /tmp
COPY target/*.jar brewery-0.0.1-SNAPSHOT
ENTRYPOINT ["java","-jar","/brewery-0.0.1-SNAPSHOT"]