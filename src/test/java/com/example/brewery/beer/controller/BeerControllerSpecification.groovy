package com.example.brewery.beer.controller

import com.example.brewery.beer.dto.BeerDTO
import com.example.brewery.beer.service.BeerService
import org.springframework.http.ResponseEntity
import spock.lang.Specification
import spock.lang.Subject

class BeerControllerSpecification extends Specification {

    def "getById should return result from findById in service wrapped in in ResponseEntity"() {
        given:
        def service = Stub(BeerService)
        service.findById(id) >> new BeerDTO(id, name, style, code)
        and: "controller with prepared stub of BeerService"
        @Subject
        def controller = new BeerController(service)

        when:
        def result = controller.getById(id)

        then:
        result.class == ResponseEntity
        result.body.id() == id
        result.body.name() == name
        result.body.style() == style
        result.body.upcCode() == code

        where:
                id        | name  | style  | code
        UUID.randomUUID() |"name" |"style" | "-1"
        UUID.randomUUID() |"name2"|"style2"| "2"
    }
}
