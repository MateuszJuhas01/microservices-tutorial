package com.example.brewery.order.daoService

import com.example.brewery.beer.daoService.BeerDaoService
import com.example.brewery.beer.model.Beer
import com.example.brewery.beer.model.BeerBatch
import com.example.brewery.order.dto.OrderCreateDTO
import com.example.brewery.order.model.OrderItem
import com.example.brewery.order.repository.OrderRepository
import spock.lang.Specification
import spock.lang.Subject

import java.time.Instant
import java.util.stream.Stream

class OrderDaoServiceSpecification extends Specification {

    def "createOrder should create order from #orderParts batches when #requiredBeers beers are required and batch size is #batchSize"() {
        given:
        def beerDaoServ = Stub(BeerDaoService)
        beerDaoServ.getBatchesToOrder(_, _, _) >>> [getBatchesWithAmount(UUID.randomUUID(), batchSize, fetchLimit),
                                                    getBatchesWithAmount(UUID.randomUUID(), batchSize, fetchLimit),
                                                    getBatchesWithAmount(UUID.randomUUID(), batchSize, fetchLimit),
                                                    getBatchesWithAmount(UUID.randomUUID(), batchSize, fetchLimit),
                                                    getBatchesWithAmount(UUID.randomUUID(), batchSize, fetchLimit)]

        def orderRepo = Stub(OrderRepository)
        orderRepo.save(_) >> { OrderItem item -> item }
        @Subject
        OrderDaoServicePostgres service = new OrderDaoServicePostgres(beerDaoServ, fetchLimit, orderRepo)

        when:
        def createdOrderItem = service.createOrder(new OrderCreateDTO(UUID.randomUUID(), requiredBeers))

        then:
        createdOrderItem.getOrderParts().size() == orderParts
        createdOrderItem.getAmount() == requiredBeers

        where:
        fetchLimit | batchSize | orderParts | requiredBeers
        1          | 1         | 5          | 5
        5          | 25        | 4          | 100
        5          | 26        | 4          | 100
        5          | 24        | 5          | 100

    }

    def "createOrder should create order with #requiredBeers beers from #fetchLimit batches and should be left #beersLeft in last batch, with #batchSize beers in batch given"() {
        given:
        def beerDaoServ = Stub(BeerDaoService)
        List<BeerBatch> batchList
        beerDaoServ.getBatchesToOrder(_, _, _) >> {
            batchList = getBatchesWithAmount(UUID.randomUUID(), batchSize, 1)
            return batchList
        }
        def orderRepo = Stub(OrderRepository)
        orderRepo.save(_) >> { OrderItem item -> item }
        @Subject
        OrderDaoServicePostgres service = new OrderDaoServicePostgres(beerDaoServ, fetchLimit, orderRepo)

        when:
        def createdOrderItem = service.createOrder(new OrderCreateDTO(UUID.randomUUID(), requiredBeers))

        then:
        createdOrderItem.getOrderParts().size() == orderParts
        createdOrderItem.getAmount() == requiredBeers
        batchList.getLast().getBeersLeft() == beersLeft

        where:
        fetchLimit | batchSize | orderParts | requiredBeers | beersLeft
        1          | 10        | 1          | 5             | 5

    }

    def "createOrder should update all 3 batches used to create order with beers left: #batch1Left, #batch2Left, #batch3Left with batch size #batchSize and #requiredBeers beers required"() {
        given:
        def beerDaoServ = Stub(BeerDaoService)
        List<BeerBatch> batchList
        beerDaoServ.getBatchesToOrder(_, _, _) >> getBatchesWithAmount(UUID.randomUUID(), batchSize, 3)
        beerDaoServ.saveAll(*_) >> { batches ->
            {
                batchList = batches.get(0)
                return batchList
            }
        }
        def orderRepo = Stub(OrderRepository)
        orderRepo.save(_) >> { OrderItem item -> item }
        @Subject
        OrderDaoServicePostgres service = new OrderDaoServicePostgres(beerDaoServ, 3, orderRepo)

        when:
        def createdOrderItem = service.createOrder(new OrderCreateDTO(UUID.randomUUID(), requiredBeers))

        then:
        createdOrderItem.getAmount() == requiredBeers
        batchList.get(0).getBeersLeft() == batch1Left
        batchList.get(1).getBeersLeft() == batch2Left
        batchList.get(2).getBeersLeft() == batch3Left

        where:
        batch1Left | batch2Left | batch3Left | batchSize | requiredBeers
        1          | 10         | 10         | 10        | 9
        0          | 5          | 10         | 10        | 15
        0          | 0          | 5          | 10        | 25
        0          | 0          | 0          | 10        | 30

    }

    private static List<BeerBatch> getBatchesWithAmount(UUID id, Integer amount, Integer batchesCount) {
        return Stream.generate {
            new BeerBatch(id, new Beer(), amount, amount, Instant.now())
        }.limit(batchesCount).toList();
    }
}
