package com.example.brewery.order.model;

import com.example.brewery.beer.model.Beer;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import java.util.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Entity
public class OrderItem {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(columnDefinition = "VARCHAR(255)")
    private UUID id;
    @ManyToOne
    @JoinColumn(nullable = false)
    private Beer beer;
    @Column(nullable = false)
    private int amount;
    @OneToMany(mappedBy = "orderItem", cascade = CascadeType.ALL)
    private Set<OrderPart> orderParts;

    public void addOrderPart(OrderPart orderPart){
        if(Objects.isNull(orderParts)){
            orderParts = new LinkedHashSet<>();
        }
        orderParts.add(orderPart);
    }
}
