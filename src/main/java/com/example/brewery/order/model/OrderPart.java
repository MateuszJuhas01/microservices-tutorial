package com.example.brewery.order.model;

import com.example.brewery.beer.model.BeerBatch;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import java.util.UUID;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Entity
public class OrderPart {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(columnDefinition = "VARCHAR(255)")
    private UUID id;
    @ManyToOne
    @JoinColumn(nullable = false)
    private OrderItem orderItem;
    @OneToOne
    @JoinColumn(nullable = false)
    private BeerBatch batch;
    @Column(nullable = false)
    private int amount;
}
