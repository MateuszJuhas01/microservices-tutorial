package com.example.brewery.order.repository;

import com.example.brewery.order.model.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OrderRepositoryPostgres extends OrderRepository, JpaRepository<OrderItem, UUID> {


}
