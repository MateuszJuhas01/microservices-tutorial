package com.example.brewery.order.repository;

import com.example.brewery.order.model.OrderItem;

import java.util.Optional;
import java.util.UUID;

public interface OrderRepository {

    OrderItem save(OrderItem orderItem);

    Optional<OrderItem> findById(UUID id);

}
