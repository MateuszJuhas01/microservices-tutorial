package com.example.brewery.order.dto;

import lombok.experimental.FieldNameConstants;

import java.util.UUID;

@FieldNameConstants
public record OrderCreateDTO (
    UUID beerID,
    int amount
){
}
