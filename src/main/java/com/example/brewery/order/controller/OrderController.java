package com.example.brewery.order.controller;


import com.example.brewery.order.dto.OrderCreateDTO;
import com.example.brewery.order.dto.OrderItemDTO;
import com.example.brewery.order.service.OrderService;
import com.example.brewery.order.validator.OrderCreateValidator;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService service;
    private final OrderCreateValidator createValidator;

    @InitBinder("orderCreateDTO")
    protected void bindCreateValidator(WebDataBinder binder){
        binder.setValidator(createValidator);
    }

    @Operation(summary = "creates order if required amount is available",
            responses = {@ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = OrderItemDTO.class),
                    mediaType = "application/json"))})
    @PostMapping
    public ResponseEntity<OrderItemDTO> createOrder(@Valid OrderCreateDTO createDTO){
        return ResponseEntity.ok(service.createOrder(createDTO));
    }
}
