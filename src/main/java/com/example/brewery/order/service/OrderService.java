package com.example.brewery.order.service;

import com.example.brewery.order.daoService.OrderDaoService;
import com.example.brewery.order.dto.OrderCreateDTO;
import com.example.brewery.order.dto.OrderItemDTO;
import com.example.brewery.order.mapper.OrderMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderDaoService daoService;
    private final OrderMapper mapper;

    public OrderItemDTO createOrder(OrderCreateDTO createDTO){
        return mapper.toDto(daoService.createOrder(createDTO));
    }
}
