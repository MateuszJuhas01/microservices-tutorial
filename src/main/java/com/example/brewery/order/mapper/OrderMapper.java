package com.example.brewery.order.mapper;

import com.example.brewery.beer.mapper.BeerMapper;
import com.example.brewery.order.dto.OrderItemDTO;
import com.example.brewery.order.model.OrderItem;
import org.mapstruct.Mapper;

@Mapper(uses = {BeerMapper.class})
public interface OrderMapper {

    OrderItemDTO toDto(OrderItem orderItem);
}
