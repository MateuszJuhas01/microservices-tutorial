package com.example.brewery.order.validator;

import com.example.brewery.beer.service.BeerService;
import com.example.brewery.order.dto.OrderCreateDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class OrderCreateValidator implements Validator {

    private final BeerService beerService;

    @Override
    public boolean supports(Class<?> clazz) {
        return OrderCreateDTO.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        OrderCreateDTO createDTO = (OrderCreateDTO) target;
        validateBeerId(createDTO.beerID(), errors);
        validateAmount(createDTO, errors);
    }

    private void validateBeerId(UUID beerID, Errors errors) {
        if(!beerService.existsById(beerID)){
            errors.rejectValue(OrderCreateDTO.Fields.beerID, "invalid-beer-id", "Invalid beer id");
        }
    }

    private void validateAmount(OrderCreateDTO createDTO, Errors errors){
        if(!beerService.isBeerAmountAvailable(createDTO.beerID(), createDTO.amount())){
            errors.rejectValue(OrderCreateDTO.Fields.amount, "amount-not-available", "Amount in stock is too low to place order");
        }
    }
}
