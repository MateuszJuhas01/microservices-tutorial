package com.example.brewery.order.daoService;

import com.example.brewery.beer.model.Beer;
import com.example.brewery.order.dto.OrderCreateDTO;
import com.example.brewery.order.model.OrderItem;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.UUID;

@Service
@ConditionalOnProperty(prefix = "beer", name = "dao-impl", havingValue = "dummy")
@RequiredArgsConstructor
class OrderDaoServiceDummy implements OrderDaoService{
    @Override
    public OrderItem createOrder(OrderCreateDTO order) {
        return new OrderItem(UUID.randomUUID(), new Beer(), 0, Collections.emptySet());
    }
}
