package com.example.brewery.order.daoService;

import com.example.brewery.order.dto.OrderCreateDTO;
import com.example.brewery.order.model.OrderItem;

public interface OrderDaoService {
    OrderItem createOrder(OrderCreateDTO order);
}
