package com.example.brewery.order.daoService;

import com.example.brewery.beer.daoService.BeerDaoService;
import com.example.brewery.beer.model.BeerBatch;
import com.example.brewery.exception.NotEnoughInStockException;
import com.example.brewery.order.dto.OrderCreateDTO;
import com.example.brewery.order.model.OrderItem;
import com.example.brewery.order.model.OrderPart;
import com.example.brewery.order.repository.OrderRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@Service
@ConditionalOnProperty(prefix = "beer", name = "dao-impl", havingValue = "postgres")
class OrderDaoServicePostgres implements OrderDaoService {

    private final BeerDaoService beerDaoService;
    private final int fetchBatchesToOrderLimit;
    private final OrderRepository repository;

    public OrderDaoServicePostgres(BeerDaoService beerDaoService,
                                   @Value("${beer-batch.fetch-batches-to-order-limit}") int fetchBatchesToOrderLimit,
                                   OrderRepository repository) {
        this.beerDaoService = beerDaoService;
        this.fetchBatchesToOrderLimit = fetchBatchesToOrderLimit;
        this.repository = repository;
    }

    @Override
    @Transactional
    public OrderItem createOrder(OrderCreateDTO createDTO) {
        int beersAdded = 0;
        List<BeerBatch> allBatchesFetched = new LinkedList<>();
        OrderItem orderItem = OrderItem.builder().amount(createDTO.amount()).beer(beerDaoService.findById(createDTO.beerID())).build();
        while (beersAdded != createDTO.amount()) {
            List<BeerBatch> batchesToAdd = beerDaoService.getBatchesToOrder(createDTO.beerID(), fetchBatchesToOrderLimit, allBatchesFetched.stream().map(BeerBatch::getId).toList());
            if(batchesToAdd.isEmpty()){
                throw new NotEnoughInStockException("Not enough beers in stock to fulfill the order");
            }
            beersAdded = addBeersToOrder(orderItem, batchesToAdd.iterator(), createDTO.amount(), beersAdded);
            allBatchesFetched.addAll(batchesToAdd);
        }
        beerDaoService.saveAll(allBatchesFetched);
        return repository.save(orderItem);
    }

    private int addBeersToOrder(OrderItem orderItem, Iterator<BeerBatch> iterator, int requiredAmount, int addedAmount) {
        if (!iterator.hasNext()) {
            return addedAmount;
        }
        BeerBatch beerBatch = iterator.next();
        if (beerBatch.getBeersLeft() >= requiredAmount - addedAmount) {
            orderItem.addOrderPart(OrderPart.builder().batch(beerBatch).orderItem(orderItem).amount(requiredAmount-addedAmount).build());
            beerBatch.setBeersLeft(beerBatch.getBeersLeft()-(requiredAmount-addedAmount));
            return requiredAmount;
        }
        orderItem.addOrderPart(OrderPart.builder().batch(beerBatch).orderItem(orderItem).amount(beerBatch.getBeersLeft()).build());
        int addedFromBatch = beerBatch.getBeersLeft();
        beerBatch.setBeersLeft(0);
        return addBeersToOrder(orderItem, iterator, requiredAmount, addedAmount+addedFromBatch);
    }
}
