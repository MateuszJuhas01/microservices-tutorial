package com.example.brewery.beer.controller;

import com.example.brewery.beer.dto.BeerDTO;
import com.example.brewery.beer.dto.BeerQuantityDTO;
import com.example.brewery.beer.dto.BeerRowDTO;
import com.example.brewery.beer.searchCriteria.BeerRowCriteria;
import com.example.brewery.beer.service.BeerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/beer")
@RequiredArgsConstructor
public class BeerController {

    private final BeerService service;

    @Operation(summary = "returns beer with given id or 404 status",
                responses = {@ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = BeerDTO.class),
                                            mediaType = "application/json")),
                             @ApiResponse(responseCode = "404", description = "given id not exists")})
    @GetMapping("/{id}")
    public ResponseEntity<BeerDTO> getById(@PathVariable UUID id){
        return ResponseEntity.ok(service.findById(id));
    }

    @Operation(summary = "returns available quantity of beer with given id or 404 status if beer not exists",
            responses = {@ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = BeerQuantityDTO.class),
                    mediaType = "application/json")),
                    @ApiResponse(responseCode = "404", description = "given id not exists")})
    @GetMapping("/quantity/{id}")
    public ResponseEntity<BeerQuantityDTO> getAvailableBeerQuantity(@PathVariable UUID id){
        return ResponseEntity.ok(service.getQuantity(id));
    }

    @Operation(summary = "returns available beers",
            responses = {@ApiResponse(responseCode = "200", content = @Content(array = @ArraySchema(schema = @Schema(implementation = BeerRowDTO.class)),
                    mediaType = "application/json"))})
    @GetMapping
    public ResponseEntity<Page<BeerRowDTO>> getAvailableBeers(@PageableDefault(size = 20, sort = "name") @ParameterObject Pageable pageable,
                                                              @ParameterObject BeerRowCriteria beerRowCriteria){
        return ResponseEntity.ok(service.getAvailableBeers(pageable, beerRowCriteria));
    }


}
