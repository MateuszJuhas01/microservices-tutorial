package com.example.brewery.beer.service;

import com.example.brewery.beer.daoService.BeerDaoService;
import com.example.brewery.beer.dto.BeerDTO;
import com.example.brewery.beer.dto.BeerQuantityDTO;
import com.example.brewery.beer.dto.BeerRowDTO;
import com.example.brewery.beer.mapper.BeerMapper;
import com.example.brewery.beer.searchCriteria.BeerRowCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class BeerService {

    private final BeerDaoService daoService;
    private final BeerMapper mapper;

    public BeerDTO findById(UUID id) {
        return mapper.toDto(daoService.findById(id));
    }

    public BeerQuantityDTO getQuantity(UUID id) {
        return daoService.getAvailableQuantity(id);
    }

    public Page<BeerRowDTO> getAvailableBeers(Pageable pageable, BeerRowCriteria beerRowCriteria) {
        return daoService.findAllAvailable(pageable, beerRowCriteria).map(mapper::toDto);
    }

    public boolean isBeerAmountAvailable(UUID beerId, int amount) {
        return daoService.isAmountAvailable(beerId, amount);
    }

    public boolean existsById(UUID beerId) {
        return daoService.existsById(beerId);
    }
}
