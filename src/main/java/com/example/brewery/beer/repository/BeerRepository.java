package com.example.brewery.beer.repository;

import com.example.brewery.beer.model.Beer;
import com.example.brewery.beer.model.BeerQuantity;

import java.util.Optional;
import java.util.UUID;

public interface BeerRepository {
    Optional<Beer> findById (UUID id);

    Beer save(Beer beer);

    BeerQuantity getBeerQuantity(UUID id);

    boolean existsById(UUID beerId);
}
