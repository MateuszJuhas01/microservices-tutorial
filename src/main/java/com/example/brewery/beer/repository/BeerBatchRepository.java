package com.example.brewery.beer.repository;

import com.example.brewery.beer.model.BeerBatch;
import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface BeerBatchRepository extends JpaRepository<BeerBatch, UUID> {

    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("""
            SELECT batch FROM BeerBatch batch
            where batch.beer.id = :beerId
            and batch.beersLeft >  0
            and batch.id not in :fetchedIds
            ORDER BY batch.createDate ASC
            LIMIT :limit
            """)
    List<BeerBatch> getBatchesToOrder(UUID beerId, int limit, List<UUID> fetchedIds);
}
