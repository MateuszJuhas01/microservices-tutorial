package com.example.brewery.beer.repository;

import com.example.brewery.beer.model.Beer;
import com.example.brewery.beer.model.BeerQuantity;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

@ConditionalOnProperty(prefix = "beer", name = "dao-impl", havingValue = "postgres")
interface BeerPostgresRepository extends BeerRepository, JpaRepository<Beer, UUID>{

    //todo add currently in production calculation
    @Query("""
            SELECT new com.example.brewery.beer.model.BeerQuantity(
                 SUM(batch.beersLeft),
                 Sum(0))
            FROM BeerBatch batch
            WHERE batch.beer.id = :id
        """)
    BeerQuantity getBeerQuantity(UUID id);
}
