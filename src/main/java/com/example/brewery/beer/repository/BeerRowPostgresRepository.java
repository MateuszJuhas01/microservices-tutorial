package com.example.brewery.beer.repository;

import com.example.brewery.beer.model.BeerRow;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.UUID;

public interface BeerRowPostgresRepository extends JpaRepository<BeerRow, UUID>,JpaSpecificationExecutor<BeerRow> {

    @Query("SELECT CASE WHEN row.availableQuantity > :amount THEN true ELSE false END FROM BeerRow row WHERE row.id = :beerId")
    boolean isBeerAmountAvailable(UUID beerId, int amount);

    interface Specs{
        static Specification<BeerRow> byName(String name){
            return (root, query, builder) -> builder.like(builder.lower(root.get(BeerRow.Fields.name)), getLikeVal(name));
        }

        static Specification<BeerRow> byStyle(String style){
            return (root, query, builder) -> builder.like(builder.lower(root.get(BeerRow.Fields.style)), getLikeVal(style));
        }

        static Specification<BeerRow> byQuantity(int quantity){
            return (root, query, builder) -> builder.greaterThanOrEqualTo(root.get(BeerRow.Fields.availableQuantity), quantity);
        }

        private static String getLikeVal(String val){
            return "%"+val.toLowerCase()+"%";
        }
    }
}
