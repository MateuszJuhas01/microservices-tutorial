package com.example.brewery.beer.searchCriteria;

public record BeerRowCriteria(
        String name,
        String style,
        Integer quantity
){
}
