package com.example.brewery.beer.dto;

import java.util.UUID;

public record BeerDTO (
        UUID id,
        String name,
        String style,
        String upcCode
){
}
