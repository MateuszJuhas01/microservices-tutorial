package com.example.brewery.beer.dto;

import java.util.UUID;

public record BeerRowDTO(
        UUID id,
        String name,
        String style,
        int availableQuantity
) {
}
