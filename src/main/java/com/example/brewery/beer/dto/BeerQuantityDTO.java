package com.example.brewery.beer.dto;

public record BeerQuantityDTO(
        int available,
        int currentlyInProduction
) {
}
