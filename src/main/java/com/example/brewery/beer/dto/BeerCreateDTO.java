package com.example.brewery.beer.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.experimental.FieldNameConstants;

@FieldNameConstants
public record BeerCreateDTO(
        @NotBlank String name,
        @NotBlank String style,
        @NotBlank String upcCode
) {
}
