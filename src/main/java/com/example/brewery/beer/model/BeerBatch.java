package com.example.brewery.beer.model;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.GenericGenerator;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@FieldNameConstants
@Entity
public class BeerBatch {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", columnDefinition = "VARCHAR(255)")
    private UUID id;
    @ManyToOne
    @JoinColumn(nullable = false)
    private Beer beer;
    @Setter
    @Column(nullable = false)
    private int beersLeft;
    @Column(nullable = false)
    private int batchSize;
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Instant createDate;
}
