package com.example.brewery.beer.model;


import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class BeerQuantity{
        private Long available;
        private Long currentlyInProduction;
}
