package com.example.brewery.beer.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;
import org.hibernate.annotations.Immutable;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Entity
@Immutable
@FieldNameConstants
public class BeerRow {
    @Id
    @Column(columnDefinition = "VARCHAR(255)")
    private UUID id;
    private String name;
    private String style;
    private Long availableQuantity;
}
