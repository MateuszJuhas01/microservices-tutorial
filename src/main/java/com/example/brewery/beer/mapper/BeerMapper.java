package com.example.brewery.beer.mapper;

import com.example.brewery.beer.dto.BeerDTO;
import com.example.brewery.beer.dto.BeerQuantityDTO;
import com.example.brewery.beer.dto.BeerRowDTO;
import com.example.brewery.beer.model.Beer;
import com.example.brewery.beer.model.BeerQuantity;
import com.example.brewery.beer.model.BeerRow;
import com.example.brewery.beer.model.BeerStyle;
import org.mapstruct.Mapper;

@Mapper
public interface BeerMapper {
    BeerDTO toDto(Beer beer);

    BeerQuantityDTO quantityToDTO(BeerQuantity quantity);

    BeerRowDTO toDto(BeerRow row);

    default String styleToString(BeerStyle style){
        return style.getName();
    }
}
