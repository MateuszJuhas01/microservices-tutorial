package com.example.brewery.beer.daoService;

import com.example.brewery.beer.dto.BeerQuantityDTO;
import com.example.brewery.beer.mapper.BeerMapper;
import com.example.brewery.beer.model.Beer;
import com.example.brewery.beer.model.BeerBatch;
import com.example.brewery.beer.model.BeerRow;
import com.example.brewery.beer.repository.BeerBatchRepository;
import com.example.brewery.beer.repository.BeerRepository;
import com.example.brewery.beer.repository.BeerRowPostgresRepository;
import com.example.brewery.beer.repository.BeerRowPostgresRepository.Specs;
import com.example.brewery.beer.searchCriteria.BeerRowCriteria;
import com.example.brewery.exception.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Service
@ConditionalOnProperty(prefix = "beer", name = "dao-impl", havingValue = "postgres")
@RequiredArgsConstructor
class BeerDaoServicePostgres implements BeerDaoService {

    private final BeerRepository beerRepository;
    private final BeerMapper mapper;
    private final BeerRowPostgresRepository beerRowRepository;
    private final BeerBatchRepository batchRepository;

    @Override
    public Beer findById(UUID id) {
        return beerRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(Beer.class, id));
    }

    @Override
    public Beer save(Beer dao) {
        return new Beer(UUID.randomUUID(), dao.getName(), dao.getStyle(), dao.getUpcCode(), true);
    }

    @Override
    public BeerQuantityDTO getAvailableQuantity(UUID id) {
        return mapper.quantityToDTO(beerRepository.getBeerQuantity(id));
    }

    @Override
    public Page<BeerRow> findAllAvailable(Pageable pageable, BeerRowCriteria criteria) {
        Specification<BeerRow> spec = Specification.where(null);
        if(StringUtils.isNotBlank(criteria.name())){
            spec = spec.and(Specs.byName(criteria.name()));
        }
        if(StringUtils.isNotBlank(criteria.style())){
            spec = spec.and(Specs.byStyle(criteria.style()));
        }
        if(!Objects.isNull(criteria.quantity())){
            spec = spec.and(Specs.byQuantity(criteria.quantity()));
        }
        return beerRowRepository.findAll(spec, pageable);
    }

    @Override
    public boolean isAmountAvailable(UUID beerId, int amount) {
        return beerRowRepository.isBeerAmountAvailable(beerId, amount);
    }

    @Override
    public List<BeerBatch> getBatchesToOrder(UUID beerId, int limit, List<UUID> fetchedIds) {
        return batchRepository.getBatchesToOrder(beerId, limit, fetchedIds);
    }

    @Override
    public List<BeerBatch> saveAll(List<BeerBatch> beerBatch) {
        return batchRepository.saveAll(beerBatch);
    }

    @Override
    public boolean existsById(UUID beerId) {
        return beerRepository.existsById(beerId);
    }


}
