package com.example.brewery.beer.daoService;

import com.example.brewery.beer.dto.BeerQuantityDTO;
import com.example.brewery.beer.model.Beer;
import com.example.brewery.beer.model.BeerBatch;
import com.example.brewery.beer.model.BeerRow;
import com.example.brewery.beer.model.BeerStyle;
import com.example.brewery.beer.searchCriteria.BeerRowCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
@ConditionalOnProperty(prefix = "beer", name = "dao-impl", havingValue = "dummy")
@RequiredArgsConstructor
class BeerDaoServiceDummy implements BeerDaoService {

    @Override
    public Beer findById(UUID id) {
        return new Beer(id, "dummy beer", new BeerStyle(UUID.randomUUID(), "dummy style", 1, "dummy"), "-1", true);
    }

    @Override
    public Beer save(Beer dao) {
        return new Beer(UUID.randomUUID(), dao.getName(), dao.getStyle(), dao.getUpcCode(), true);
    }

    @Override
    public BeerQuantityDTO getAvailableQuantity(UUID id) {
        return new BeerQuantityDTO(0,0);
    }

    @Override
    public Page<BeerRow> findAllAvailable(Pageable pageable, BeerRowCriteria beerRowCriteria) {
        return Page.empty();
    }

    @Override
    public boolean isAmountAvailable(UUID beerId, int amount) {
        return false;
    }

    @Override
    public List<BeerBatch> getBatchesToOrder(UUID beerId, int limit, List<UUID> fetchedIds) {
        return Collections.emptyList();
    }

    @Override
    public List<BeerBatch> saveAll(List<BeerBatch> beerBatch) {
        return Collections.emptyList();
    }

    @Override
    public boolean existsById(UUID beerId) {
        return false;
    }
}
