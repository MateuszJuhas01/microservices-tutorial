package com.example.brewery.beer.daoService;

import com.example.brewery.beer.dto.BeerQuantityDTO;
import com.example.brewery.beer.model.Beer;
import com.example.brewery.beer.model.BeerBatch;
import com.example.brewery.beer.model.BeerRow;
import com.example.brewery.beer.searchCriteria.BeerRowCriteria;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.UUID;

public interface BeerDaoService {

    Beer findById(UUID id);

    Beer save(Beer dao);

    BeerQuantityDTO getAvailableQuantity(UUID id);

    Page<BeerRow> findAllAvailable(Pageable pageable, BeerRowCriteria beerRowCriteria);

    boolean isAmountAvailable(UUID beerId, int amount);

    List<BeerBatch> getBatchesToOrder(UUID beerId, int limit, List<UUID> fetchedIds);

    List<BeerBatch> saveAll(List<BeerBatch> beerBatch);

    boolean existsById(UUID beerId);
}
