package com.example.brewery.exception;

import java.util.UUID;

public class ResourceNotFoundException extends RuntimeException{

    public ResourceNotFoundException(Class c, UUID id) {
        super(String.format("Resource of type: %s and id: %s was not found", c.getSimpleName(), id.toString()));
    }
}
