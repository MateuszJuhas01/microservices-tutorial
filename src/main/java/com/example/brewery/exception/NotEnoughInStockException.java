package com.example.brewery.exception;

public class NotEnoughInStockException extends RuntimeException{
    public NotEnoughInStockException(String message) {
        super(message);
    }
}
